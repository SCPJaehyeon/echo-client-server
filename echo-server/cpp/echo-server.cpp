#include "header/echo-server.h"
using namespace std;

void usage(char *argv){
    printf("Usage : %s [port] [+options] \n", argv);
    printf("Example) %s 1234 -e -b \n", argv);
}

void *th_sock(void *arg_v){
    Args *arg_use = (Args *)arg_v;
    int socket_client = arg_use->sock_client;
    int read_chk;
    char buff[256];

    list<int>* sock_id = arg_use->sock_id;
    list<int>::iterator socketit;

    while(1){
        memset(buff, 0, 256);
        read_chk = read(socket_client, buff, 255);
        if(read_chk < 0){
            cout << "read end" << endl;
            break;
        }
        buff[strlen(buff)] = '\n';
        cout << buff;

        if(arg_use->option_echo == true){
            cout << "echo!" << endl;
            write(socket_client, buff, int(sizeof(buff))-1);
        }

        if(arg_use->option_broad == true){
            arg_use->m.lock();
            for(socketit = sock_id->begin(); socketit != sock_id->end(); socketit++){
                cout << "broad!" << endl;
                write(*socketit, buff, int(sizeof(buff))-1);
            }
            arg_use->m.unlock();
        }
    }
    arg_use->m.lock();
    for(socketit = sock_id->begin(); socketit != sock_id->end(); socketit++){
        if(*socketit==socket_client){
            sock_id->erase(socketit);
        }
    }
    arg_use->m.unlock();
    close(socket_client);
    free(arg_v);

    return 0;
}
